const socialGlobalName = `roygrssmn`;

module.exports = {
  siteMetadata: {
    title: `Testbrew`,
    author: `Roy Grossman`,
    description: `Brewing some new and old tests.`,
    siteUrl: `https://testbrew.com`,
    social: {
      twitter: socialGlobalName,
      linkedin: `roy-g`,
      github: socialGlobalName,
      gitlab: socialGlobalName,
    },
    pathPrefix: `/website`,
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
               trackingId: 'G-HT2P722E2Y',
      },
    },
    `gatsby-plugin-feed`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `TestBrew - Brewing tests`,
        short_name: `TestBrew`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#0053a4`,
        display: `standalone`,
        lang: `en`,
        crossOrigin: `anonymous`, // `use-credentials` or `anonymous`
        icon: `content/assets/testbrew-minimal-icon.png`,
        cache_busting_mode: `name`,
        icon_options: {
          purpose: `any`,
        },
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-dark-mode`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
};
