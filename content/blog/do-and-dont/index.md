---
title: Interview Do's and Don'ts
date: '2020-04-21T20:10:03.284Z'
description: 'What are the basic do and do not before and during an interview'
published: true
tags: ['interview']
excerpt: None
---

#### Seeking positions - dos and don'ts
I had the opportunity in my short career to recruit people from around the globe, 
it was a learning process for me, which taught me a lot. 
I thought I could share some experiences with you so you can better prepare for your next interview.   
Feel free to agree, disagree and contact me if you have more.

####Do:
Might be that all (or some) of these you do! which is great, but if you don't then start doing!
- - -
- Do check that you know and understand all the technologies on your CV, 
don’t say you know Java but in truth you took only one course in a semester.   
- Do prepare before and ask yourself some interview questions on the technologies listed in your CV.   
- Do check what the company does, try to understand, if you don't understand then ask in the interview.   
- Do _freshen up_ before the interview, yes first impression is important, if you had to run to your interview, 
  fix yourself up (this applies to the interviewer as well 😄 ).   
- Do repeat the question asked if you think the question is not clear, this isn’t disrespecting, on the opposite, 
  it assures that you are into details.   
- In remote interviews, make sure your internet connection is stable, and you can hear and be heard. 
  It is bad if you are asked about one question but because you heard clearly you answer something not related.   
- Do go through the company's Glassdoor before coming to the interview, this will better prepare you.   
- Do talk about your expectations, an interview is a match for you as well as it is for the company. 
  So be open and talk about yourself.   
- Do come with a positive attitude, it shows on your face and body language.   
- **Be yourself!**
####Don’ts
Some don'ts here will shock you...    
I have witnessed these in person, so please, try not to do the same mistakes as others.
- - -
- Don’t be late, arrive 15 min before, this is a good buffer.
- Don’t give only one interviewer all of your attention, focus on everyone in the room. 
  I had seen some candidates who only talked and looked at one interviewer, ignoring all others.
- Don’t contradict yourself, if you said X and believe in X, then explain why you believe in X, 
  remember that you and the interviewer can disagree, the interviewer wants to see your thought process.
- Don't badmouth former jobs, technologies or employees, try to explain what you didn’t like there and why.
- Don’t try to hurry to your next meeting, if you have other engagements at the end of the interview, 
  let the interviewers know beforehand.
- Don’t deflect questions, if you don’t know something then it is better to say I don’t know then to answer something else.


<p>
<br> <br>
Do you have any other recommendations? feel free to get in touch!
</p>