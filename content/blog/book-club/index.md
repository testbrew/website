---
title: Book Club
date: '2020-04-19T10:10:03.284Z'
description: 'Recommended list of books that I think might benefit others'
published: true
tags: ['books', 'bookclub', 'reading']
excerpt: None
---

### My book club
My 2020 year resolution was to read more books. 
The kind of books that can make me sit down and think and wonder, 
books that will inspire me to be better in tech, at work, and in life.     
Luckily I have good friends who recommend some books. 
I thought it would be great to return the favour and share my 
recommendation with you with an explanation why I think you 
should also read them. 

#### [The Unicorn Project]
Top of my list is **The Unicorn project**. The book is an IT novel, 
but the story is so common it feels like an autobiography, 
as the story is close to home. The story is about a company that has 
an IT department which is working so bad it causes the company 
to almost go bankrupt. The story takes you through the process 
of becoming a better company with a great IT department.
This is great for anyone who wants to understand the mindset of 
DevOps and how software collaboration can be better. 
It is also a nice book for people who want to have a better 
understanding of how IT departments work.

#### [Radical Candor]
This was recommended to me as a book on developing myself to be a 
great manager, but the fact is that this book can make you a better person. 
The author describes how to be a "Radical Candor", no spoilers, 
but it will change the way you communicate at work and at home. 
A great read for anyone who wants to succeed in creating a good 
working environment.

#### [The DevOps Handbook]
If you are new to DevOps then you must read this book, 
it has all the basic (and advanced) knowledge to move you into the 
DevOps world. In my opinion this is the book to read after you are 
done with [the Unicorn Project]. The book breaks the DevOps world 
into simple building blocks and examples which you can follow.

#### [Liquid Software]
Fred, Yoav and Baruch explain how to make your software release more fluid, 
hence the title, liquid software. It will take you through the process of releasing 
in a shorter interval with great quality and by doing so, building trust with your end users.   
If you want to deliver more value at a fast pace, then you have to read this book!

![book picture](./books.JPG)

#### [Modern Operating Systems] 
Are you a geek that wants to better understand your operating system? 
then this book is for you. It was the first IT book I read, 
and it is still my favorite! 

<p>
<br> <br>
Do you have any other recommendations? feel free to get in touch!
</p>

[The DevOps Handbook]: https://www.amazon.com/dp/B01M9ASFQ3
[The Unicorn Project]: https://www.amazon.com/dp/B07QT9QR41
[Radical Candor]: https://www.amazon.com/dp/B07XVQB7XV
[Liquid Software]: https://liquidsoftware.com/
[Modern Operating Systems]: https://www.amazon.com/dp/9332575770/ 
