#### Some reference:
- I've used [LogRocker] blog as an inspiration for this website. Check them out!   
- [gutsby with typescript] blog also used this to get some tags inspiration.   
- And credits to this [great demo].   
- Also found [this].   

###### TODO: convert all tsx to ts if you can   
  
[LogRocker] : https://blog.logrocket.com/set-up-a-typescript-gatsby-app/
[gutsby with typescript] : https://jeffrafter.com/gatsby-with-typescript/
[great demo] : https://gatsby-starter-blog-demo.netlify.com/
[this]: https://www.settletom.com/blog/how-to-gatsby-and-graphcms-blog